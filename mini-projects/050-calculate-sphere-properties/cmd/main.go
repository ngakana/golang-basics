package main

import (
	"fmt"

	calculate "bitbucket.com/salemaneinthebucket/golang-basics/mini-projects/050-calculate-sphere-properties"
)

func main() {
	area, err := calculate.Area()
	vol, _ := calculate.Volume()
	surfArea, _ := calculate.SurfaceArea()

	if err == nil {
		fmt.Println("Your sphere properties:")
		fmt.Printf("Radius: %.2f units\n", calculate.Rad)
		fmt.Printf("Area: %.2f square units\n", area)
		fmt.Printf("Volume: %.2f cubic units\n", vol)
		fmt.Printf("Surface area: %.2f square units\n", surfArea)
	} else {
		fmt.Println(err)
	}
}
