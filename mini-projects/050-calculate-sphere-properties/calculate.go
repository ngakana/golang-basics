package calculate

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

var a = os.Args

func isValid() bool {
	var valid bool = false
	if len(a) > 1 {
		var _, err = strconv.ParseFloat(a[1], 64)
		if err == nil {
			valid = true
		}
	}
	return valid
}

var Rad, _ = strconv.ParseFloat(a[1], 64)

func Area() (float64, error) {
	if isValid() {
		return math.Pow(Rad, 2) * math.Pi, nil
	} else {
		return 0, fmt.Errorf("IputError! %v is not a valid radius", a[1])
	}
}

func Volume() (float64, error) {
	if isValid() {
		return (4 / 3) * math.Pi * math.Pow(Rad, 3), nil
	} else {
		return 0, fmt.Errorf("IputError! %v is not a valid radius", a[1])
	}
}

func SurfaceArea() (float64, error) {
	if isValid() {
		return 4 * math.Pow(Rad, 2) * math.Pi, nil
	} else {
		return 0, fmt.Errorf("IputError! %v is not a valid radius", a[1])
	}
}
