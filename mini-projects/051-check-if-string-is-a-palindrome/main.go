package main

import (
	"fmt"
	"os"
)

var a = os.Args

func isValid() bool {
	var valid bool = false
	if len(a) > 1 {
		valid = true
	}
	return valid
}

var s string = a[1]

func isPalindrome() bool {
	isPalindrome := false

	if isValid() {

		l := len(s)
		if l == 0 || l == 1 {
			isPalindrome = true
			return isPalindrome
		} else if l%2 == 0 {
			return isPalindrome
		}

		left := 0
		right := l - 1

		for {
			if left == right {
				isPalindrome = true
				return isPalindrome
			} else if s[left] == s[right] {
				left++
				right--
				continue
			} else {
				return isPalindrome
			}
		}
	}

	return isPalindrome
}

func main() {
	if isPalindrome() {
		fmt.Printf("`%s` is a palindrome\n", s)
	} else {
		fmt.Printf("`%s` is not a palindrome\n", s)
	}
}
