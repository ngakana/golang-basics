package main

import (
	"fmt"

	tempconv "bitbucket.com/salemaneinthebucket/golang-basics/mini-projects/060-comprehensive-temperature-converter"
)

func main() {
	var temp, err = tempconv.Compute()
	if err == nil {
		if tempconv.ConvDelta {
			fmt.Printf("%.2f %s change = %.1f %s change\n", tempconv.A2, tempconv.InitUnits, temp, tempconv.ToUnits)
		} else {
			fmt.Printf("%f %s = %.1f %s\n", tempconv.A2, tempconv.InitUnits, temp, tempconv.ToUnits)
		}
	} else {
		fmt.Println(err)
	}
}
