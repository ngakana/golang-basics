package tempconv

/*
 Todo
 [x] CELCIUS <--> KELVIN
	[x] temp[K] = 273.15 + t[degC]
	[x] temp[degC] = t[K] - 273.15
	[x] deltaT[degC] = deltaT[K]
 [x] FAHRENHEIT <--> RANKINE
	[X] temp[degF] = t[degR] - 459.67
	[x] temp[degR] = 459.67 + t[degF]
	[x] deltaT[degF] = deltaT[degR]
 [x] CELCIUS <--> FAHRENHEIT
	[x] temp[degC] = (1/1.8)*(t[degF] - 32)
	[x] temp[degF] = 1.8*t[degC] + 32
	[x] deltaT[degC] = (1/1.8)*deltaT[degF]
	[x] deltaT[degF] = 1.8*deltaT[degC]
 [x] KELVIN <--> RANKINE
	[x] deltaT[K] = (10/18)*deltaT[degR]
	[x] deltaT[degR] = 1.8*deltaT[K]
*/

// LIBRARIE
import (
	"fmt"
	"strconv"
)

// CONSTANTS
const c2k float32 = 273.15 // Degree Celcius to Kelvin diff.
const f2r = 459.67         // Degree Fahrenheit to Degree diff.
const c2f_delta = 1.8      // Change in temp. in Degree Celcius to Degree Fahrenheit
const c2f_zero = 32.0      // Degree celcius zero to Degree Fahrenheit zero diff.

// GLOBAL VARIABLES
var a1 int
var A2 float32
var ConvDelta bool = false
var InitUnits string
var ToUnits string

// FUNCTION DEFINITIONS
func getStdIn() string {
	var s string

	fmt.Println(
		"Please indicate required operation(type number only):\n",
		"1  --> Convert temperature from Celsius to Fahrenheit\n",
		"2  --> Convert temperature from Fahrenheit to Celsius\n",
		"3  --> Convert temperature from Celsius to Kelvin\n",
		"4  --> Convert temperature from Kelvin to Celsius\n",
		"5  --> Convert temperature from Fahrenheit to Rankine\n",
		"6  --> Convert temperature from Rankine to Fahrenheit\n\n",

		"7  --> Convert temperature change from Celsius to Fahrenheit\n",
		"8  --> Convert temperature change from Fahrenheit to Celsius\n",
		"9  --> Convert temperature change from Celsius to Kelvin\n",
		"10 --> Convert temperature change from Kelvin to Celsius\n",
		"11 --> Convert temperature change from Fahrenheit to Rankine\n",
		"12 --> Convert temperature change from Rankine to Fahrenheit",
	)
	fmt.Scanf("%s", &s)
	a, err := strconv.ParseInt(s, 0, 8)

	if err == nil && a >= 1 && a <= 12 {
		switch a {
		case 1, 3, 7, 9:
			fmt.Println("Enter temperature value [Degree Celcius]:")
			fmt.Scanf("%f", &A2)
		case 2, 5, 8, 11:
			fmt.Println("Enter temperature value [Degree Fahrenheit]")
			fmt.Scanf("%f", &A2)
		case 4, 10:
			fmt.Println("Enter temperature value [Kelvin]")
			fmt.Scanf("%f", &A2)
		case 6, 12:
			fmt.Println("Enter temperature value [Degree Rankine]")
			fmt.Scanf("%f", &A2)
		}
	} else {

		for {
			fmt.Println("That value is invalid, please try again")
			fmt.Scanf("%s", &s)
			a, err := strconv.ParseInt(s, 0, 8)

			if err == nil {
				switch a {
				case 1, 3, 7, 9:
					fmt.Println("Enter temperature value [Degree Celcius]:")
					fmt.Scanf("%f", &A2)
				case 2, 5, 8, 11:
					fmt.Println("Enter temperature value [Degree Fahrenheit]")
					fmt.Scanf("%f", &A2)
				case 4, 10:
					fmt.Println("Enter temperature value [Kelvin]")
					fmt.Scanf("%f", &A2)
				case 6, 12:
					fmt.Println("Enter temperature value [Degree Rankine]")
					fmt.Scanf("%f", &A2)
				}
				break
			}
		}
	}
	return s
}

func isValid() bool {

	s := getStdIn()
	var (
		a   int
		err error
	)

	a, err = strconv.Atoi(s)

	var valid bool = false
	if a >= 1 && a <= 12 {
		if err == nil {
			a1 = a
			valid = true
		}
	}
	return valid
}

func Compute() (float32, error) {
	var temp float32

	if isValid() {
		switch a1 {
		case 1: // celcius --> fahrenheit = 1.8*t[degC] + 32
			temp = c2f_delta*A2 + c2f_zero
			InitUnits = "Degree Celcius"
			ToUnits = "Degree Fahrenheit"
		case 2: // celcius <-- fahrenheit = (1/1.8)*(t[degF] - 32)
			temp = (1 / c2f_delta) * (A2 - c2f_zero)
			InitUnits = "Degree Fahrenheit"
			ToUnits = "Degree Celcius"
		case 3: // celcius --> kelvin = 273.15 + t[degC]
			temp = A2 + c2k
			InitUnits = "Degree Celcius"
			ToUnits = "Kelvin"
		case 4: // celcius <-- kelvin = t[K] - 273.15
			temp = A2 - c2k
			InitUnits = "Kelvin"
			ToUnits = "Degree Celcius"
		case 5: // fahrenheit --> rankine = 459.67 + t[degF]
			temp = A2 + f2r
			InitUnits = "Degree Fahrenheit"
			ToUnits = "Degree Rankine"
		case 6: // fahrenheit <-- rankine = t[degF] - 459.67
			temp = A2 - f2r
			InitUnits = "Degree Rankine"
			ToUnits = "Degree Fahrenheit"

		case 7: // delta_celcius --> delta_fahrenheit | deltaT[degF] = 1.8*deltaT[degC]
			temp = c2f_delta * A2
			InitUnits = "Degree Celcius"
			ToUnits = "Degree Fahrenheit"
			ConvDelta = true
		case 8: // delta_celcius <-- delta_fahrenheit | deltaT[degC] = (1/1.8)*deltaT[degF]
			temp = (1 / c2f_delta) * A2
			InitUnits = "Degree Fahrenheit"
			ToUnits = "Degree Celcius"
			ConvDelta = true
		case 9: // delta_celcius --> delta_kelvin | deltaT[degC] = deltaT[K]
			temp = A2
			InitUnits = "Degree Celcius"
			ToUnits = "Kelvin"
			ConvDelta = true
		case 10: // delta_celcius <-- delta_kelvin | deltaT[degC] = deltaT[K]
			temp = A2
			InitUnits = "Kelvin"
			ToUnits = "Degree Celcius"
			ConvDelta = true
		case 11: // fahrenheit --> delta_rankine = 459.67 + t[degF]
			temp = A2
			InitUnits = "Degree Fahrenheit"
			ToUnits = "Degree Rankine"
			ConvDelta = true
		case 12: // fahrenheit <-- rankine = t[degF] - 459.67
			temp = A2
			InitUnits = "Degree Rankine"
			ToUnits = "Degree Fahrenheit"
			ConvDelta = true
		}
	} else {
		return 0, fmt.Errorf("couldn't convert %f from %s to %s", A2, InitUnits, ToUnits)
	}
	return temp, nil
}
