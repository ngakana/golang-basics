package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Print the literals
//
//  1. Print a few integer literals
//
//  2. Print a few float literals
//
//  3. Print true and false bool constants
//
//  4. Print your name using a string literal
//
//  5. Print a non-english sentence using a string literal
//
// ---------------------------------------------------------

func main() {
	fmt.Println(
		-5, -2, 0, 4, 7,
	)
	fmt.Println(
		-3.141592, -2.718281828, 2.718281828, 3.141592,
	)
	fmt.Println(
		false, true,
	)
	fmt.Println("We're just getting started 🥳! Coming for you Luno!")
}
