package main

import (
	"fmt"

	mygolang "bitbucket.com/salemaneinthebucket/golang-basics/03-custom-library"
)

func main() {
	fmt.Println("Go version:", mygolang.Version())
}
