package mygolang

import (
	"runtime"
)

// Version returns the Go version running
func Version() string {
	return runtime.Version()
}
