package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Shy Semicolons
//
//  1. Try to type your statements by separating them using
//     semicolons
//
//  2. Observe how Go fixes them for you
//
// ---------------------------------------------------------

func main() {
	fmt.Println("This program was typed out as follows:\n\npackage main;\n\nimport \"fmt\";\n\nfunc main(){\n\tfmt.Println(\"This line was typed out as follows\");\n}\n\nBut Go formatted it to what you see in the source code")
}
