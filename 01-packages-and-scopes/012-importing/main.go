package main

// ---------------------------------------------------------
// EXERCISE: Rename imports
//
//  1- Import fmt package three times with different names
//
//  2- Print a few messages using those imports
//
// EXPECTED OUTPUT
//  hello
//  hey
//  hi
// ---------------------------------------------------------

import (
	"fmt"
	log "fmt"
	p "fmt"
)

func main() {
	fmt.Println("hello")
	p.Println("hey")
	log.Println("hi")
}
